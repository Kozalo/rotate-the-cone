﻿namespace ru.kozalo.ismart.cone
{
    partial class Lab2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Lab2));
            this.oGLobj = new Tao.Platform.Windows.SimpleOpenGlControl();
            this.RunBtn = new System.Windows.Forms.Button();
            this.rotateTimer = new System.Windows.Forms.Timer(this.components);
            this.controlPanel = new System.Windows.Forms.Panel();
            this.SettingsButton = new System.Windows.Forms.Button();
            this.SetColorButton = new System.Windows.Forms.Button();
            this.RadiusLabel = new System.Windows.Forms.Label();
            this.HeightLabel = new System.Windows.Forms.Label();
            this.defaultButton = new System.Windows.Forms.Button();
            this.AngleValue = new System.Windows.Forms.NumericUpDown();
            this.AngleLabel = new System.Windows.Forms.Label();
            this.Zvalue = new System.Windows.Forms.NumericUpDown();
            this.Yvalue = new System.Windows.Forms.NumericUpDown();
            this.Xvalue = new System.Windows.Forms.NumericUpDown();
            this.Zlabel = new System.Windows.Forms.Label();
            this.Ylabel = new System.Windows.Forms.Label();
            this.Xlabel = new System.Windows.Forms.Label();
            this.HeightTrack = new System.Windows.Forms.TrackBar();
            this.RadiusTrack = new System.Windows.Forms.TrackBar();
            this.ColorDialog = new System.Windows.Forms.ColorDialog();
            this.ToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.controlPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AngleValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Zvalue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Yvalue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Xvalue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HeightTrack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadiusTrack)).BeginInit();
            this.SuspendLayout();
            // 
            // oGLobj
            // 
            this.oGLobj.AccumBits = ((byte)(0));
            this.oGLobj.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.oGLobj.AutoCheckErrors = false;
            this.oGLobj.AutoFinish = false;
            this.oGLobj.AutoMakeCurrent = true;
            this.oGLobj.AutoSwapBuffers = true;
            this.oGLobj.BackColor = System.Drawing.Color.White;
            this.oGLobj.ColorBits = ((byte)(32));
            this.oGLobj.DepthBits = ((byte)(16));
            this.oGLobj.Location = new System.Drawing.Point(-1, 84);
            this.oGLobj.Name = "oGLobj";
            this.oGLobj.Size = new System.Drawing.Size(481, 358);
            this.oGLobj.StencilBits = ((byte)(0));
            this.oGLobj.TabIndex = 0;
            this.oGLobj.MouseDown += new System.Windows.Forms.MouseEventHandler(this.oGLobj_MouseDown);
            this.oGLobj.MouseMove += new System.Windows.Forms.MouseEventHandler(this.oGLobj_MouseMove);
            this.oGLobj.MouseUp += new System.Windows.Forms.MouseEventHandler(this.oGLobj_MouseUp);
            // 
            // RunBtn
            // 
            this.RunBtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.RunBtn.Location = new System.Drawing.Point(60, 66);
            this.RunBtn.Name = "RunBtn";
            this.RunBtn.Size = new System.Drawing.Size(245, 23);
            this.RunBtn.TabIndex = 1;
            this.RunBtn.Text = "Визуализировать!";
            this.RunBtn.UseVisualStyleBackColor = true;
            this.RunBtn.Click += new System.EventHandler(this.RunBtn_Click);
            // 
            // rotateTimer
            // 
            this.rotateTimer.Interval = 20;
            this.rotateTimer.Tick += new System.EventHandler(this.rotateTimer_Tick);
            // 
            // controlPanel
            // 
            this.controlPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.controlPanel.Controls.Add(this.SettingsButton);
            this.controlPanel.Controls.Add(this.SetColorButton);
            this.controlPanel.Controls.Add(this.RadiusLabel);
            this.controlPanel.Controls.Add(this.HeightLabel);
            this.controlPanel.Controls.Add(this.defaultButton);
            this.controlPanel.Controls.Add(this.AngleValue);
            this.controlPanel.Controls.Add(this.AngleLabel);
            this.controlPanel.Controls.Add(this.Zvalue);
            this.controlPanel.Controls.Add(this.Yvalue);
            this.controlPanel.Controls.Add(this.RunBtn);
            this.controlPanel.Controls.Add(this.Xvalue);
            this.controlPanel.Controls.Add(this.Zlabel);
            this.controlPanel.Controls.Add(this.Ylabel);
            this.controlPanel.Controls.Add(this.Xlabel);
            this.controlPanel.Controls.Add(this.HeightTrack);
            this.controlPanel.Controls.Add(this.RadiusTrack);
            this.controlPanel.Location = new System.Drawing.Point(-1, 0);
            this.controlPanel.Name = "controlPanel";
            this.controlPanel.Size = new System.Drawing.Size(481, 92);
            this.controlPanel.TabIndex = 2;
            // 
            // SettingsButton
            // 
            this.SettingsButton.Location = new System.Drawing.Point(1, 66);
            this.SettingsButton.Name = "SettingsButton";
            this.SettingsButton.Size = new System.Drawing.Size(60, 23);
            this.SettingsButton.TabIndex = 16;
            this.SettingsButton.Text = "Опции...";
            this.ToolTip.SetToolTip(this.SettingsButton, "Настройки для более опытных пользователей");
            this.SettingsButton.UseVisualStyleBackColor = true;
            this.SettingsButton.Click += new System.EventHandler(this.OptionsButton_Click);
            // 
            // SetColorButton
            // 
            this.SetColorButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SetColorButton.Location = new System.Drawing.Point(304, 66);
            this.SetColorButton.Name = "SetColorButton";
            this.SetColorButton.Size = new System.Drawing.Size(76, 23);
            this.SetColorButton.TabIndex = 15;
            this.SetColorButton.Text = "Цвет...";
            this.ToolTip.SetToolTip(this.SetColorButton, "Задать основной цвет конуса");
            this.SetColorButton.UseVisualStyleBackColor = true;
            this.SetColorButton.Click += new System.EventHandler(this.SetColorButton_Click);
            // 
            // RadiusLabel
            // 
            this.RadiusLabel.AutoSize = true;
            this.RadiusLabel.Location = new System.Drawing.Point(241, 43);
            this.RadiusLabel.Name = "RadiusLabel";
            this.RadiusLabel.Size = new System.Drawing.Size(46, 13);
            this.RadiusLabel.TabIndex = 14;
            this.RadiusLabel.Text = "Радиус:";
            // 
            // HeightLabel
            // 
            this.HeightLabel.AutoSize = true;
            this.HeightLabel.Location = new System.Drawing.Point(13, 43);
            this.HeightLabel.Name = "HeightLabel";
            this.HeightLabel.Size = new System.Drawing.Size(48, 13);
            this.HeightLabel.TabIndex = 13;
            this.HeightLabel.Text = "Высота:";
            // 
            // defaultButton
            // 
            this.defaultButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.defaultButton.Location = new System.Drawing.Point(379, 66);
            this.defaultButton.Name = "defaultButton";
            this.defaultButton.Size = new System.Drawing.Size(102, 23);
            this.defaultButton.TabIndex = 10;
            this.defaultButton.Text = "Сброс";
            this.ToolTip.SetToolTip(this.defaultButton, "Сбросить всё по умолчанию");
            this.defaultButton.UseVisualStyleBackColor = true;
            this.defaultButton.Click += new System.EventHandler(this.defaultButton_Click);
            // 
            // AngleValue
            // 
            this.AngleValue.Location = new System.Drawing.Point(412, 14);
            this.AngleValue.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.AngleValue.Minimum = new decimal(new int[] {
            60,
            0,
            0,
            -2147483648});
            this.AngleValue.Name = "AngleValue";
            this.AngleValue.Size = new System.Drawing.Size(50, 20);
            this.AngleValue.TabIndex = 9;
            this.ToolTip.SetToolTip(this.AngleValue, "Скорость вращения");
            this.AngleValue.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // AngleLabel
            // 
            this.AngleLabel.AutoSize = true;
            this.AngleLabel.Location = new System.Drawing.Point(321, 17);
            this.AngleLabel.Name = "AngleLabel";
            this.AngleLabel.Size = new System.Drawing.Size(85, 13);
            this.AngleLabel.TabIndex = 8;
            this.AngleLabel.Text = "Угол поворота:";
            // 
            // Zvalue
            // 
            this.Zvalue.Location = new System.Drawing.Point(242, 14);
            this.Zvalue.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.Zvalue.Name = "Zvalue";
            this.Zvalue.Size = new System.Drawing.Size(50, 20);
            this.Zvalue.TabIndex = 7;
            this.ToolTip.SetToolTip(this.Zvalue, "Z координата вектора вращения");
            // 
            // Yvalue
            // 
            this.Yvalue.Location = new System.Drawing.Point(137, 14);
            this.Yvalue.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.Yvalue.Name = "Yvalue";
            this.Yvalue.Size = new System.Drawing.Size(50, 20);
            this.Yvalue.TabIndex = 6;
            this.ToolTip.SetToolTip(this.Yvalue, "Y координата вектора вращения");
            this.Yvalue.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // Xvalue
            // 
            this.Xvalue.Location = new System.Drawing.Point(37, 14);
            this.Xvalue.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.Xvalue.Name = "Xvalue";
            this.Xvalue.Size = new System.Drawing.Size(50, 20);
            this.Xvalue.TabIndex = 5;
            this.ToolTip.SetToolTip(this.Xvalue, "X координата вектора вращения");
            this.Xvalue.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // Zlabel
            // 
            this.Zlabel.AutoSize = true;
            this.Zlabel.Location = new System.Drawing.Point(217, 18);
            this.Zlabel.Name = "Zlabel";
            this.Zlabel.Size = new System.Drawing.Size(17, 13);
            this.Zlabel.TabIndex = 4;
            this.Zlabel.Text = "Z:";
            // 
            // Ylabel
            // 
            this.Ylabel.AutoSize = true;
            this.Ylabel.Location = new System.Drawing.Point(114, 18);
            this.Ylabel.Name = "Ylabel";
            this.Ylabel.Size = new System.Drawing.Size(17, 13);
            this.Ylabel.TabIndex = 3;
            this.Ylabel.Text = "Y:";
            // 
            // Xlabel
            // 
            this.Xlabel.AutoSize = true;
            this.Xlabel.Location = new System.Drawing.Point(13, 17);
            this.Xlabel.Name = "Xlabel";
            this.Xlabel.Size = new System.Drawing.Size(17, 13);
            this.Xlabel.TabIndex = 2;
            this.Xlabel.Text = "X:";
            // 
            // HeightTrack
            // 
            this.HeightTrack.LargeChange = 1;
            this.HeightTrack.Location = new System.Drawing.Point(54, 41);
            this.HeightTrack.Maximum = 1000;
            this.HeightTrack.Name = "HeightTrack";
            this.HeightTrack.Size = new System.Drawing.Size(185, 45);
            this.HeightTrack.TabIndex = 11;
            this.HeightTrack.TickStyle = System.Windows.Forms.TickStyle.None;
            this.HeightTrack.Value = 300;
            this.HeightTrack.Scroll += new System.EventHandler(this.Track_Scroll);
            // 
            // RadiusTrack
            // 
            this.RadiusTrack.LargeChange = 1;
            this.RadiusTrack.Location = new System.Drawing.Point(284, 41);
            this.RadiusTrack.Maximum = 1000;
            this.RadiusTrack.Name = "RadiusTrack";
            this.RadiusTrack.Size = new System.Drawing.Size(185, 45);
            this.RadiusTrack.SmallChange = 5;
            this.RadiusTrack.TabIndex = 12;
            this.RadiusTrack.TickStyle = System.Windows.Forms.TickStyle.None;
            this.RadiusTrack.Value = 100;
            this.RadiusTrack.Scroll += new System.EventHandler(this.Track_Scroll);
            // 
            // ColorDialog
            // 
            this.ColorDialog.Color = System.Drawing.Color.Violet;
            // 
            // Lab2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(479, 441);
            this.Controls.Add(this.controlPanel);
            this.Controls.Add(this.oGLobj);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(495, 450);
            this.Name = "Lab2";
            this.Text = "Лабораторная работа №2. Вращение конуса";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Resize += new System.EventHandler(this.Lab2_Resize);
            this.controlPanel.ResumeLayout(false);
            this.controlPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AngleValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Zvalue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Yvalue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Xvalue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HeightTrack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadiusTrack)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Tao.Platform.Windows.SimpleOpenGlControl oGLobj;
        private System.Windows.Forms.Button RunBtn;
        private System.Windows.Forms.Timer rotateTimer;
        private System.Windows.Forms.Panel controlPanel;
        private System.Windows.Forms.Label Zlabel;
        private System.Windows.Forms.Label Ylabel;
        private System.Windows.Forms.Label Xlabel;
        private System.Windows.Forms.NumericUpDown Xvalue;
        private System.Windows.Forms.NumericUpDown Zvalue;
        private System.Windows.Forms.NumericUpDown Yvalue;
        private System.Windows.Forms.NumericUpDown AngleValue;
        private System.Windows.Forms.Label AngleLabel;
        private System.Windows.Forms.Button defaultButton;
        private System.Windows.Forms.TrackBar RadiusTrack;
        private System.Windows.Forms.TrackBar HeightTrack;
        private System.Windows.Forms.Label RadiusLabel;
        private System.Windows.Forms.Label HeightLabel;
        private System.Windows.Forms.ColorDialog ColorDialog;
        private System.Windows.Forms.Button SetColorButton;
        private System.Windows.Forms.Button SettingsButton;
        private System.Windows.Forms.ToolTip ToolTip;
    }
}

