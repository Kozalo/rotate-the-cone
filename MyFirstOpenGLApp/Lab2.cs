﻿using System;
using System.Drawing;
using System.Windows.Forms;

using Tao.FreeGlut;
using Tao.OpenGl;
using Tao.Platform.Windows;

namespace ru.kozalo.ismart.cone
{
    public partial class Lab2 : Form
    {
        // Все параметры по умолчанию указаны в defaultButton_Click

        // Все эти переменные используются только для вращения фигуры мышью
        private LastPos _mLastPos;
        private int _alpha = 0, _beta = 0;    // сюда заносятся значения для смещения фигуры
        private bool _mousePressed = false;

        // И только эта используется везде
        private ConeParams _mConeParams;

        // Только для второй реализации создания конуса!
        private int _glListIndex = 0;

        private SettingsForm _mSettingsForm;
        private RenderingParams _currentRenderingParams;

        public Lab2()
        {
            InitializeComponent();
            oGLobj.InitializeContexts();
            oGLobj.MouseWheel += oGLobj_MouseWheel;

            _mConeParams.Height = HeightTrack.Value / 100;
            _mConeParams.Radius = RadiusTrack.Value / 100;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // Инициализация OpenGL
            Glut.glutInit();
            Glut.glutInitDisplayMode(Glut.GLUT_RGB | Glut.GLUT_DOUBLE | Glut.GLUT_DEPTH);
            Gl.glClearColor(255, 255, 255, 1);
            Gl.glViewport(0, 0, oGLobj.Width, oGLobj.Height);
            Gl.glMatrixMode(Gl.GL_PROJECTION);
            Gl.glLoadIdentity();
            Glu.gluPerspective(45, (float)oGLobj.Width / (float)oGLobj.Height, 0.1, 200);
            Gl.glMatrixMode(Gl.GL_MODELVIEW);
            Gl.glLoadIdentity();
            Gl.glEnable(Gl.GL_DEPTH_TEST);
            Gl.glEnable(Gl.GL_COLOR_MATERIAL);

            // Задаём начальные значения для фигуры
            Gl.glClear(Gl.GL_COLOR_BUFFER_BIT | Gl.GL_DEPTH_BUFFER_BIT);
            Gl.glLoadIdentity();
            Gl.glTranslated(0, 0, -6);  // и слега отдаляем её от камеры
            Gl.glPushMatrix();

            // Создаём окно настроек
            _mSettingsForm = new SettingsForm(this);

            // Устанавливаем параметры по умолчанию для режима построения конуса
            RenderingParamsInit(ref _currentRenderingParams);
        }

        private static void RenderingParamsInit(ref RenderingParams renderingParams)
        {
            renderingParams = new RenderingParams
            {
                Method = RenderingMethod.Polygons,
                Accuracy = 0.5f
            };
        }

        private void RunBtn_Click(object sender, EventArgs e)
        {
            if (!rotateTimer.Enabled)
            {
                RunBtn.Text = "Стоп!";
                rotateTimer.Enabled = true;
            }
            else
            {
                RunBtn.Text = "Поехали!";
                rotateTimer.Enabled = false;
            }
        }

        private void rotateTimer_Tick(object sender, EventArgs e)
        {
            if ( Xvalue.Value == 0 && Yvalue.Value == 0 && Zvalue.Value == 0 ) return;    // Если выхода не будет, то фигура будет просто уменьшаться

            Gl.glClear(Gl.GL_COLOR_BUFFER_BIT | Gl.GL_DEPTH_BUFFER_BIT);
            Gl.glPopMatrix();   // Фигура не теряется между интерациями
            Gl.glTranslated(0, 0, _mConeParams.Height / 2 - 0.2);   // Вращаем по центру
            Gl.glRotated((double) AngleValue.Value, (double) Xvalue.Value, (double) Yvalue.Value, (double) Zvalue.Value);
            Gl.glTranslated(0, 0, -(_mConeParams.Height / 2 - 0.2));    // Не забыв вернуть центр
            MakeACone(_mConeParams.Height, _mConeParams.Radius);    // Для отрисовки конуса используем собственную функцию
            Gl.glPushMatrix();
            Gl.glFlush();
            oGLobj.Invalidate();
        }

        private void Lab2_Resize(object sender, EventArgs e)
        {
            Gl.glViewport(0, 0, oGLobj.Width, oGLobj.Height);
            Gl.glClear(Gl.GL_COLOR_BUFFER_BIT);
        }

        private void oGLobj_MouseWheel(object sender, MouseEventArgs e)
        {
            // Фигуру можно масштабировать колёсиком мыши
            float delta;
            if (e.Delta > 0) delta = 1.03f;
            else if (e.Delta < 0) delta = 0.97f;
            else delta = 0;

            Gl.glClear(Gl.GL_COLOR_BUFFER_BIT | Gl.GL_DEPTH_BUFFER_BIT);
            Gl.glPopMatrix();
            Gl.glTranslated(0, 0, _mConeParams.Height / 2 - 0.2);
            Gl.glScaled(delta, delta, delta);
            Gl.glTranslated(0, 0, -(_mConeParams.Height / 2 - 0.2));
            MakeACone(_mConeParams.Height, _mConeParams.Radius);
            Gl.glPushMatrix();
            Gl.glFlush();
            oGLobj.Invalidate();
        }

        private void oGLobj_MouseMove(object sender, MouseEventArgs e)
        {
            if (!_mousePressed) return;

            // Вращаем фигуру на угол, равный разности двух положений мыши
            _alpha = e.X - _mLastPos.X;
            _beta = ((SimpleOpenGlControl) sender).Height - e.Y - _mLastPos.Y;   // Не забываем, что Y-координата в OpenGL начинается в другом месте
            _mLastPos.X = e.X;
            _mLastPos.Y = ((SimpleOpenGlControl) sender).Height - e.Y;

            Gl.glClear(Gl.GL_COLOR_BUFFER_BIT | Gl.GL_DEPTH_BUFFER_BIT);
            Gl.glPopMatrix();

            if (e.Button == MouseButtons.Left)
            {
                Gl.glTranslated(0, 0, _mConeParams.Height / 2 - 0.2);
                Gl.glRotated(_alpha, 0, 1, 0);
                Gl.glRotated(_beta, -1, 0, 0);
                Gl.glTranslated(0, 0, -(_mConeParams.Height / 2 - 0.2));
            }
            else if (e.Button == MouseButtons.Right)
            {
                Gl.glTranslated((float)_alpha / 100, (float)_beta / 100, 0);
            }

            MakeACone(_mConeParams.Height, _mConeParams.Radius);
            Gl.glPushMatrix();
            Gl.glFlush();
            oGLobj.Invalidate();
        }

        private void oGLobj_MouseDown(object sender, MouseEventArgs e)
        {
            RunBtn.Text = "Поехали!";
            rotateTimer.Enabled = false;

            _mLastPos.X = e.X;
            _mLastPos.Y = e.Y;
            _mousePressed = true;
        }

        private void oGLobj_MouseUp(object sender, MouseEventArgs e)
        {
            _mousePressed = false;
        }

        private void defaultButton_Click(object sender, EventArgs e)
        {
            // Сбрасывает все параметры программы в начальное состояние
            RunBtn.Text = "Визуализировать!";
            rotateTimer.Enabled = false;
            _mousePressed = false;

            Xvalue.Value = 1;    // Конечно, плохо, что параметры не отделены от кода в виде констант,
            Yvalue.Value = 1;    // несмотря на повторения, но я позволю себе пренебречь этим в столь небольшой программе
            Zvalue.Value = 0;
            AngleValue.Value = 5;

            HeightTrack.Value = 300;
            RadiusTrack.Value = 100;
            _mConeParams.Height = 3;
            _mConeParams.Radius = 1;

            ColorDialog.Color = Color.Violet;

            RenderingParamsInit(ref _currentRenderingParams);
            _glListIndex = 0;

            Gl.glPopMatrix();
            Gl.glLoadIdentity();
            Gl.glTranslated(0, 0, -6);
            Gl.glPushMatrix();
            Gl.glClear(Gl.GL_COLOR_BUFFER_BIT | Gl.GL_DEPTH_BUFFER_BIT);
            Gl.glFlush();
            oGLobj.Invalidate();
        }

        // Функция отрисовки конуса переписывалась несколько раз в соответствиями с рекомендациями товарища kic.a и моей интерпретацией его слов
        // с целью повышения качества рендера и увеличения функционала работы над конусом.

        private void MakeACone(float height, float radius)
        {
            if (_glListIndex == 0)
            {
                switch (_currentRenderingParams.Method)
                {
                    case RenderingMethod.Glut:
                        MakeAConeWithGlut(height, radius);
                        break;
                    case RenderingMethod.Polygons:
                        MakeAConeWithPolygons(height, radius);
                        break;
                    case RenderingMethod.Circles:
                        MakeAConeWithCircles(height, radius);
                        break;
                    default:
                        MessageBox.Show("Не задан метод построения! Попробуйте установить его в окне настроек.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        defaultButton.PerformClick();
                        SettingsButton.PerformClick();
                        return;
                }
            }

            Gl.glCallList(_glListIndex);
        }

        private void MakeAConeWithGlut(float height, float radius)
        {
            var baseRed = (float) ColorDialog.Color.R / 255;
            var baseGreen = (float) ColorDialog.Color.G / 255;
            var baseBlue = (float) ColorDialog.Color.B / 255;

            _glListIndex = Gl.glGenLists(1);
            Gl.glNewList(_glListIndex, Gl.GL_COMPILE);

            Gl.glColor3f(baseRed, baseGreen, baseBlue);
            if (_currentRenderingParams.LinesOnly)
                Glut.glutWireCone(radius, height, _currentRenderingParams.Slices, _currentRenderingParams.Stacks);
            else
                Glut.glutSolidCone(radius, height, _currentRenderingParams.Slices, _currentRenderingParams.Stacks);

            if (_currentRenderingParams.WithCircle)
            {
                Gl.glBegin(Gl.GL_TRIANGLE_FAN);
                Gl.glColor3f(baseRed - 0.3f, baseGreen - 0.3f, baseBlue - 0.3f);
                Gl.glVertex3f(0, 0, 0);

                Gl.glColor3f(baseRed - 0.2f, baseGreen - 0.2f, baseBlue - 0.2f);
                for (float angle = 0; angle < 360; angle += _currentRenderingParams.Accuracy)
                    Gl.glVertex3f((float) Math.Sin(angle) * radius, (float) Math.Cos(angle) * radius, -0.001f);
                Gl.glEnd();
            }

            Gl.glEndList();
        }

        // MakeACone (версия с полигонами)
        // Изначально для рисования конуса использовалась Glut.glutSolidCone(), но потом появилась задача изменения цвета "днища" конуса,
        // так что пришлось сделать свою, взяв за основу код со Stackoverflow (http://stackoverflow.com/questions/19245363/opengl-glut-surface-normals-of-cone).
        // Ч - честность :)
        private void MakeAConeWithPolygons(float height, float radius)
        {
            var baseRed = (float) ColorDialog.Color.R / 255;
            var baseGreen = (float) ColorDialog.Color.G / 255;
            var baseBlue = (float) ColorDialog.Color.B / 255;

            _glListIndex = Gl.glGenLists(1);
            Gl.glNewList(_glListIndex, Gl.GL_COMPILE);

            // Рисуем верхнюю часть конуса
            Gl.glBegin(Gl.GL_TRIANGLE_FAN); // Рисуем полигоны треугольниками по парам точек, изначально задав общий центр
                Gl.glColor3f(baseRed + 0.2f, baseGreen + 0.2f, baseBlue + 0.2f);     // Цвета не сплощные, а слегка градиентные: так интереснее. :)
                Gl.glVertex3f(0, 0, height);

                Gl.glColor3f(baseRed, baseGreen, baseBlue);
                for (float angle = 0; angle < 360; angle += _currentRenderingParams.Accuracy)
                    Gl.glVertex3f((float) Math.Sin(angle) * radius, (float) Math.Cos(angle) * radius, 0);
            Gl.glEnd();

            // Рисуем круг-основание
            Gl.glBegin(Gl.GL_TRIANGLE_FAN);
                Gl.glColor3f(baseRed - 0.3f, baseGreen - 0.3f, baseBlue - 0.3f);
                Gl.glVertex3f(0, 0, 0);

                Gl.glColor3f(baseRed - 0.2f, baseGreen - 0.2f, baseBlue - 0.2f);
                for (float angle = 0; angle < 360; angle += _currentRenderingParams.Accuracy)
                    Gl.glVertex3f((float) Math.Sin(angle) * radius, (float) Math.Cos(angle) * radius, 0);
            Gl.glEnd();

            Gl.glEndList();
        }

        // MakeACone (версия с множеством окружностей)
        private void MakeAConeWithCircles(float height, float radius)
        {
            var baseRed = (float) ColorDialog.Color.R / 255;
            var baseGreen = (float) ColorDialog.Color.G / 255;
            var baseBlue = (float) ColorDialog.Color.B / 255;
            var radiusIncrement = radius / (height / _currentRenderingParams.CirclesAccuracy);

            _glListIndex = Gl.glGenLists(1);
            Gl.glNewList(_glListIndex, Gl.GL_COMPILE);

            // Рисуем конус
            Gl.glLineWidth(_currentRenderingParams.LinesWidth);
            for (float h = 0, r = radius, c = 0; h < height; h += _currentRenderingParams.CirclesAccuracy)
            {
                Gl.glBegin(Gl.GL_LINE_STRIP);

                if (_currentRenderingParams.ColorInsteadLight)
                {
                    c += _currentRenderingParams.LightIncrement;
                    if ((h > (height / 2 + _currentRenderingParams.Shift - _currentRenderingParams.Range / 2)) && (h < (height / 2 + _currentRenderingParams.Shift + _currentRenderingParams.Range / 2)))
                        Gl.glColor3f(_currentRenderingParams.Colors3F[0], _currentRenderingParams.Colors3F[1], _currentRenderingParams.Colors3F[2]);
                    else
                        Gl.glColor3f(baseRed + c, baseGreen + c, baseBlue + c);
                }
                else
                {
                    if (h < (height / 2 + _currentRenderingParams.Shift)) c += _currentRenderingParams.LightIncrement;
                    else c -= _currentRenderingParams.LightIncrement;
                    Gl.glColor3f(baseRed + c, baseGreen + c, baseBlue + c);
                }                    
                    
                for (float angle = 0; angle < 360; angle += _currentRenderingParams.Accuracy)
                    Gl.glVertex3f((float) Math.Sin(angle) * r, (float) Math.Cos(angle) * r, h);
                Gl.glEnd();
                if (r > 0) r -= radiusIncrement;
            }

            // Рисуем круг-основание
            Gl.glBegin(Gl.GL_TRIANGLE_FAN);
            Gl.glColor3f(baseRed - 0.3f, baseGreen - 0.3f, baseBlue - 0.3f);
            Gl.glVertex3f(0, 0, 0);

            Gl.glColor3f(baseRed - 0.2f, baseGreen - 0.2f, baseBlue - 0.2f);
            for (float angle = 0; angle < 360; angle += _currentRenderingParams.Accuracy)
                Gl.glVertex3f((float) Math.Sin(angle) * radius, (float) Math.Cos(angle) * radius, 0);
            Gl.glEnd();

            Gl.glEndList();
        }
        //

        /* Это вариант по всем канонам многоуважаемого товарища kic.a с использованием его кода
        // Но конус нормально сделать так и не получилось. Собственно, как и раскрасить. Фигня какая-то короче

        private void MakeAConeWithQuades(float height, float radius)
        {
            float baseRed = (float)ColorDialog.Color.R / 255;
            float baseGreen = (float)ColorDialog.Color.G / 255;
            float baseBlue = (float)ColorDialog.Color.B / 255;

            int verticalSegs = 3;
            int horizontalSegs = 32;
            int vertSegHeight = 1;

            glListIndex = Gl.glGenLists(1);
            Gl.glNewList(glListIndex, Gl.GL_COMPILE);

            //Gl.glBegin(Gl.GL_LINE_STRIP);
            Gl.glBegin(Gl.GL_QUADS);
            Gl.glColor3f(baseRed + 0.2f, baseGreen + 0.2f, baseBlue + 0.2f);

            // ** http://pastebin.com/BKxLFeiQ
            for (int i = 0; i < verticalSegs; i++)
                for (double j = 0; j < Math.PI * 2; j += Math.PI / horizontalSegs)
                {
                    double x1 = Math.Cos(j) * radius;
                    double x2 = Math.Cos(j + Math.PI / horizontalSegs) * radius;
                    double z1 = Math.Sin(j) * radius;
                    double z2 = Math.Sin(j + Math.PI / horizontalSegs) * radius;
                    double y1 = vertSegHeight * i;
                    double y2 = vertSegHeight * (i + 1);
                    
                    Gl.glVertex3d(x1, y1, z1);
                    Gl.glVertex3d(x2, y1, z2);
                    Gl.glVertex3d(x2, y2, z2);
                    Gl.glVertex3d(x1, y2, z1);
                }
            // **

            Gl.glEnd();

            Gl.glBegin(Gl.GL_TRIANGLE_FAN);
            Gl.glColor3f(baseRed - 0.1f, baseGreen - 0.1f, baseBlue - 0.1f);
            Gl.glVertex3f(0, 0, 0);

            Gl.glColor3f(baseRed, baseGreen, baseBlue);
            for (float angle = 0; angle < 360; angle += 0.01f)
                Gl.glVertex3f((float)Math.Sin(angle) * radius, 0, (float)Math.Cos(angle) * radius);
            Gl.glEnd();

            Gl.glEndList();
        }
         */

        private void Track_Scroll(object sender, EventArgs e)
        {
            rotateTimer.Enabled = false;
            RunBtn.Text = "Поехали!";

            _mConeParams.Height = (float) HeightTrack.Value / 100;
            _mConeParams.Radius = (float) RadiusTrack.Value / 100;

            RedrawTheScene();
        }

        private void SetColorButton_Click(object sender, EventArgs e)
        {
            if (ColorDialog.ShowDialog() == DialogResult.OK)
                RedrawTheScene();
        }

        private void RedrawTheScene()
        {
            Gl.glClear(Gl.GL_COLOR_BUFFER_BIT | Gl.GL_DEPTH_BUFFER_BIT);
            Gl.glPopMatrix();

            Gl.glDeleteLists(_glListIndex, 1);
            _glListIndex = 0;

            MakeACone(_mConeParams.Height, _mConeParams.Radius);
            Gl.glPushMatrix();
            Gl.glFlush();
            oGLobj.Invalidate();
        }

        private void OptionsButton_Click(object sender, EventArgs e)
        {
            _mSettingsForm.Show();
        }

        public void ChangeRenderingMethod(RenderingParams renderingParams)
        {
            _currentRenderingParams = renderingParams;
            RedrawTheScene();
        }
    }

    // Вспомогательная структура для хранения координат курсора
    internal struct LastPos
    {
        public int X;
        public int Y;
    }

    // Структура для хранения параметров конуса
    internal struct ConeParams
    {
        public float Height;
        public float Radius;
    }

    public enum RenderingMethod
    {
        Glut, Polygons, Circles
    }

    public struct RenderingParams
    {
        public RenderingMethod Method;

        // Общий параметр для всех методов
        public float Accuracy;

        // Параметры для метода GLUT
        public bool LinesOnly;
        public bool WithCircle;
        public int Slices;
        public int Stacks;     

        // Параметры для метода CIRCLES
        public float CirclesAccuracy;
        public bool ColorInsteadLight;
        public float[] Colors3F;
        public float Range;
        public float Shift;
        public float LightIncrement;
        public int LinesWidth;
    }
}
