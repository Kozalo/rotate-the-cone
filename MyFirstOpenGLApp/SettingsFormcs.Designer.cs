﻿namespace ru.kozalo.ismart.cone
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.RenderingMethodsList = new System.Windows.Forms.ComboBox();
            this.ApplyButton = new System.Windows.Forms.Button();
            this.ToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.GlutPanel = new System.Windows.Forms.Panel();
            this.GlutAccuracyLabel = new System.Windows.Forms.Label();
            this.GlutStacksLabel = new System.Windows.Forms.Label();
            this.GlutSlicesLabel = new System.Windows.Forms.Label();
            this.GlutCircleAccuracyTrackBar = new System.Windows.Forms.TrackBar();
            this.GlutCircleCheckBox = new System.Windows.Forms.CheckBox();
            this.GlutStacksUpDown = new System.Windows.Forms.NumericUpDown();
            this.GlutSlicesUpDown = new System.Windows.Forms.NumericUpDown();
            this.GlutFillModeCheckBox = new System.Windows.Forms.CheckBox();
            this.GlutDescriptionLabel = new System.Windows.Forms.Label();
            this.PolygonPanel = new System.Windows.Forms.Panel();
            this.PolygonsAccuracyLabel = new System.Windows.Forms.Label();
            this.PolygonsAccuracyTrackBar = new System.Windows.Forms.TrackBar();
            this.PolygonsDescriptionLabel = new System.Windows.Forms.Label();
            this.CirclesPanel = new System.Windows.Forms.Panel();
            this.CirclesLightIncrementLabel = new System.Windows.Forms.Label();
            this.CirclesAmountAccuracyLabel = new System.Windows.Forms.Label();
            this.CirclesAccuracyLabel = new System.Windows.Forms.Label();
            this.CirclesLinesWidthLabel = new System.Windows.Forms.Label();
            this.CirclesShiftLabel = new System.Windows.Forms.Label();
            this.CirclesRangeLabel = new System.Windows.Forms.Label();
            this.CirclesLinesWidthTrackBar = new System.Windows.Forms.TrackBar();
            this.CirclesLightIncrementTrackBar = new System.Windows.Forms.TrackBar();
            this.CirclesAmountAccuracyTrackBar = new System.Windows.Forms.TrackBar();
            this.CirclesAccuracyTrackBar = new System.Windows.Forms.TrackBar();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.CirclesColorChangeModeRadioBtn = new System.Windows.Forms.RadioButton();
            this.CirclesLightChangeModeRadioBtn = new System.Windows.Forms.RadioButton();
            this.CirclesShiftTrackBar = new System.Windows.Forms.TrackBar();
            this.CirclesRangeTrackBar = new System.Windows.Forms.TrackBar();
            this.CirclesDescriptionLabel = new System.Windows.Forms.Label();
            this.SelectColorDialog = new System.Windows.Forms.ColorDialog();
            this.GlutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GlutCircleAccuracyTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GlutStacksUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GlutSlicesUpDown)).BeginInit();
            this.PolygonPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PolygonsAccuracyTrackBar)).BeginInit();
            this.CirclesPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CirclesLinesWidthTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CirclesLightIncrementTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CirclesAmountAccuracyTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CirclesAccuracyTrackBar)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CirclesShiftTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CirclesRangeTrackBar)).BeginInit();
            this.SuspendLayout();
            // 
            // RenderingMethodsList
            // 
            this.RenderingMethodsList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.RenderingMethodsList.DisplayMember = "1";
            this.RenderingMethodsList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.RenderingMethodsList.FormattingEnabled = true;
            this.RenderingMethodsList.Items.AddRange(new object[] {
            "Стандартная функция GLUT",
            "Построение по полигонам",
            "Построение окружностями"});
            this.RenderingMethodsList.Location = new System.Drawing.Point(12, 12);
            this.RenderingMethodsList.Name = "RenderingMethodsList";
            this.RenderingMethodsList.Size = new System.Drawing.Size(444, 21);
            this.RenderingMethodsList.TabIndex = 0;
            this.ToolTip.SetToolTip(this.RenderingMethodsList, "Метод построения конуса");
            this.RenderingMethodsList.ValueMember = "0";
            this.RenderingMethodsList.SelectedIndexChanged += new System.EventHandler(this.RenderingMethodsList_SelectedIndexChanged);
            // 
            // ApplyButton
            // 
            this.ApplyButton.Location = new System.Drawing.Point(195, 324);
            this.ApplyButton.Name = "ApplyButton";
            this.ApplyButton.Size = new System.Drawing.Size(75, 23);
            this.ApplyButton.TabIndex = 1;
            this.ApplyButton.Text = "Применить";
            this.ApplyButton.UseVisualStyleBackColor = true;
            this.ApplyButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // GlutPanel
            // 
            this.GlutPanel.Controls.Add(this.GlutSlicesLabel);
            this.GlutPanel.Controls.Add(this.GlutCircleAccuracyTrackBar);
            this.GlutPanel.Controls.Add(this.GlutCircleCheckBox);
            this.GlutPanel.Controls.Add(this.GlutStacksUpDown);
            this.GlutPanel.Controls.Add(this.GlutSlicesUpDown);
            this.GlutPanel.Controls.Add(this.GlutFillModeCheckBox);
            this.GlutPanel.Controls.Add(this.GlutDescriptionLabel);
            this.GlutPanel.Controls.Add(this.GlutAccuracyLabel);
            this.GlutPanel.Controls.Add(this.GlutStacksLabel);
            this.GlutPanel.Location = new System.Drawing.Point(12, 42);
            this.GlutPanel.Name = "GlutPanel";
            this.GlutPanel.Size = new System.Drawing.Size(445, 270);
            this.GlutPanel.TabIndex = 2;
            this.GlutPanel.Visible = false;
            // 
            // GlutAccuracyLabel
            // 
            this.GlutAccuracyLabel.AutoSize = true;
            this.GlutAccuracyLabel.Location = new System.Drawing.Point(13, 191);
            this.GlutAccuracyLabel.Name = "GlutAccuracyLabel";
            this.GlutAccuracyLabel.Size = new System.Drawing.Size(150, 13);
            this.GlutAccuracyLabel.TabIndex = 8;
            this.GlutAccuracyLabel.Text = "Точность построения круга:";
            // 
            // GlutStacksLabel
            // 
            this.GlutStacksLabel.AutoSize = true;
            this.GlutStacksLabel.Location = new System.Drawing.Point(13, 157);
            this.GlutStacksLabel.Name = "GlutStacksLabel";
            this.GlutStacksLabel.Size = new System.Drawing.Size(122, 13);
            this.GlutStacksLabel.TabIndex = 7;
            this.GlutStacksLabel.Text = "Вертикальных секций:";
            // 
            // GlutSlicesLabel
            // 
            this.GlutSlicesLabel.AutoSize = true;
            this.GlutSlicesLabel.Location = new System.Drawing.Point(13, 121);
            this.GlutSlicesLabel.Name = "GlutSlicesLabel";
            this.GlutSlicesLabel.Size = new System.Drawing.Size(133, 13);
            this.GlutSlicesLabel.TabIndex = 6;
            this.GlutSlicesLabel.Text = "Горизонтальных секций:";
            // 
            // GlutCircleAccuracyTrackBar
            // 
            this.GlutCircleAccuracyTrackBar.Enabled = false;
            this.GlutCircleAccuracyTrackBar.Location = new System.Drawing.Point(14, 212);
            this.GlutCircleAccuracyTrackBar.Maximum = 100;
            this.GlutCircleAccuracyTrackBar.Minimum = 1;
            this.GlutCircleAccuracyTrackBar.Name = "GlutCircleAccuracyTrackBar";
            this.GlutCircleAccuracyTrackBar.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.GlutCircleAccuracyTrackBar.Size = new System.Drawing.Size(339, 45);
            this.GlutCircleAccuracyTrackBar.TabIndex = 5;
            this.GlutCircleAccuracyTrackBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this.GlutCircleAccuracyTrackBar.Value = 25;
            // 
            // GlutCircleCheckBox
            // 
            this.GlutCircleCheckBox.AutoSize = true;
            this.GlutCircleCheckBox.Location = new System.Drawing.Point(241, 156);
            this.GlutCircleCheckBox.Name = "GlutCircleCheckBox";
            this.GlutCircleCheckBox.Size = new System.Drawing.Size(97, 17);
            this.GlutCircleCheckBox.TabIndex = 4;
            this.GlutCircleCheckBox.Text = "Выделить дно";
            this.GlutCircleCheckBox.UseVisualStyleBackColor = true;
            this.GlutCircleCheckBox.CheckedChanged += new System.EventHandler(this.GlutCircleCheckBox_CheckedChanged);
            // 
            // GlutStacksUpDown
            // 
            this.GlutStacksUpDown.Location = new System.Drawing.Point(152, 154);
            this.GlutStacksUpDown.Name = "GlutStacksUpDown";
            this.GlutStacksUpDown.Size = new System.Drawing.Size(57, 20);
            this.GlutStacksUpDown.TabIndex = 3;
            this.GlutStacksUpDown.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            // 
            // GlutSlicesUpDown
            // 
            this.GlutSlicesUpDown.Location = new System.Drawing.Point(152, 118);
            this.GlutSlicesUpDown.Name = "GlutSlicesUpDown";
            this.GlutSlicesUpDown.Size = new System.Drawing.Size(57, 20);
            this.GlutSlicesUpDown.TabIndex = 2;
            this.GlutSlicesUpDown.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            // 
            // GlutFillModeCheckBox
            // 
            this.GlutFillModeCheckBox.AutoSize = true;
            this.GlutFillModeCheckBox.Location = new System.Drawing.Point(241, 119);
            this.GlutFillModeCheckBox.Name = "GlutFillModeCheckBox";
            this.GlutFillModeCheckBox.Size = new System.Drawing.Size(102, 17);
            this.GlutFillModeCheckBox.TabIndex = 1;
            this.GlutFillModeCheckBox.Text = "Только каркас";
            this.GlutFillModeCheckBox.UseVisualStyleBackColor = true;
            // 
            // GlutDescriptionLabel
            // 
            this.GlutDescriptionLabel.AutoSize = true;
            this.GlutDescriptionLabel.Location = new System.Drawing.Point(7, 13);
            this.GlutDescriptionLabel.Name = "GlutDescriptionLabel";
            this.GlutDescriptionLabel.Size = new System.Drawing.Size(430, 78);
            this.GlutDescriptionLabel.TabIndex = 0;
            this.GlutDescriptionLabel.Text = resources.GetString("GlutDescriptionLabel.Text");
            // 
            // PolygonPanel
            // 
            this.PolygonPanel.Controls.Add(this.PolygonsAccuracyLabel);
            this.PolygonPanel.Controls.Add(this.PolygonsAccuracyTrackBar);
            this.PolygonPanel.Controls.Add(this.PolygonsDescriptionLabel);
            this.PolygonPanel.Location = new System.Drawing.Point(552, 42);
            this.PolygonPanel.Name = "PolygonPanel";
            this.PolygonPanel.Size = new System.Drawing.Size(444, 270);
            this.PolygonPanel.TabIndex = 3;
            this.PolygonPanel.Visible = false;
            // 
            // PolygonsAccuracyLabel
            // 
            this.PolygonsAccuracyLabel.AutoSize = true;
            this.PolygonsAccuracyLabel.Location = new System.Drawing.Point(15, 84);
            this.PolygonsAccuracyLabel.Name = "PolygonsAccuracyLabel";
            this.PolygonsAccuracyLabel.Size = new System.Drawing.Size(157, 13);
            this.PolygonsAccuracyLabel.TabIndex = 9;
            this.PolygonsAccuracyLabel.Text = "Точность построения конуса:";
            // 
            // PolygonsAccuracyTrackBar
            // 
            this.PolygonsAccuracyTrackBar.Location = new System.Drawing.Point(18, 110);
            this.PolygonsAccuracyTrackBar.Maximum = 1000;
            this.PolygonsAccuracyTrackBar.Minimum = 1;
            this.PolygonsAccuracyTrackBar.Name = "PolygonsAccuracyTrackBar";
            this.PolygonsAccuracyTrackBar.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.PolygonsAccuracyTrackBar.Size = new System.Drawing.Size(411, 45);
            this.PolygonsAccuracyTrackBar.TabIndex = 2;
            this.PolygonsAccuracyTrackBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this.PolygonsAccuracyTrackBar.Value = 50;
            // 
            // PolygonsDescriptionLabel
            // 
            this.PolygonsDescriptionLabel.AutoSize = true;
            this.PolygonsDescriptionLabel.Location = new System.Drawing.Point(15, 13);
            this.PolygonsDescriptionLabel.Name = "PolygonsDescriptionLabel";
            this.PolygonsDescriptionLabel.Size = new System.Drawing.Size(414, 39);
            this.PolygonsDescriptionLabel.TabIndex = 1;
            this.PolygonsDescriptionLabel.Text = resources.GetString("PolygonsDescriptionLabel.Text");
            // 
            // CirclesPanel
            // 
            this.CirclesPanel.Controls.Add(this.CirclesLightIncrementLabel);
            this.CirclesPanel.Controls.Add(this.CirclesAmountAccuracyLabel);
            this.CirclesPanel.Controls.Add(this.CirclesAccuracyLabel);
            this.CirclesPanel.Controls.Add(this.CirclesLinesWidthLabel);
            this.CirclesPanel.Controls.Add(this.CirclesShiftLabel);
            this.CirclesPanel.Controls.Add(this.CirclesRangeLabel);
            this.CirclesPanel.Controls.Add(this.CirclesLinesWidthTrackBar);
            this.CirclesPanel.Controls.Add(this.CirclesLightIncrementTrackBar);
            this.CirclesPanel.Controls.Add(this.CirclesAmountAccuracyTrackBar);
            this.CirclesPanel.Controls.Add(this.CirclesAccuracyTrackBar);
            this.CirclesPanel.Controls.Add(this.groupBox1);
            this.CirclesPanel.Controls.Add(this.CirclesShiftTrackBar);
            this.CirclesPanel.Controls.Add(this.CirclesRangeTrackBar);
            this.CirclesPanel.Controls.Add(this.CirclesDescriptionLabel);
            this.CirclesPanel.Location = new System.Drawing.Point(13, 397);
            this.CirclesPanel.Name = "CirclesPanel";
            this.CirclesPanel.Size = new System.Drawing.Size(445, 270);
            this.CirclesPanel.TabIndex = 4;
            // 
            // CirclesLightIncrementLabel
            // 
            this.CirclesLightIncrementLabel.AutoSize = true;
            this.CirclesLightIncrementLabel.Location = new System.Drawing.Point(293, 156);
            this.CirclesLightIncrementLabel.Name = "CirclesLightIncrementLabel";
            this.CirclesLightIncrementLabel.Size = new System.Drawing.Size(132, 13);
            this.CirclesLightIncrementLabel.TabIndex = 16;
            this.CirclesLightIncrementLabel.Text = "Интенсивность яркости:";
            // 
            // CirclesAmountAccuracyLabel
            // 
            this.CirclesAmountAccuracyLabel.AutoSize = true;
            this.CirclesAmountAccuracyLabel.Location = new System.Drawing.Point(12, 100);
            this.CirclesAmountAccuracyLabel.Name = "CirclesAmountAccuracyLabel";
            this.CirclesAmountAccuracyLabel.Size = new System.Drawing.Size(221, 13);
            this.CirclesAmountAccuracyLabel.TabIndex = 15;
            this.CirclesAmountAccuracyLabel.Text = "Количество окружностей для построения:";
            // 
            // CirclesAccuracyLabel
            // 
            this.CirclesAccuracyLabel.AutoSize = true;
            this.CirclesAccuracyLabel.Location = new System.Drawing.Point(12, 156);
            this.CirclesAccuracyLabel.Name = "CirclesAccuracyLabel";
            this.CirclesAccuracyLabel.Size = new System.Drawing.Size(157, 13);
            this.CirclesAccuracyLabel.TabIndex = 14;
            this.CirclesAccuracyLabel.Text = "Точность построения конуса:";
            // 
            // CirclesLinesWidthLabel
            // 
            this.CirclesLinesWidthLabel.AutoSize = true;
            this.CirclesLinesWidthLabel.Location = new System.Drawing.Point(12, 212);
            this.CirclesLinesWidthLabel.Name = "CirclesLinesWidthLabel";
            this.CirclesLinesWidthLabel.Size = new System.Drawing.Size(89, 13);
            this.CirclesLinesWidthLabel.TabIndex = 13;
            this.CirclesLinesWidthLabel.Text = "Толщина линий:";
            // 
            // CirclesShiftLabel
            // 
            this.CirclesShiftLabel.AutoSize = true;
            this.CirclesShiftLabel.Location = new System.Drawing.Point(309, 212);
            this.CirclesShiftLabel.Name = "CirclesShiftLabel";
            this.CirclesShiftLabel.Size = new System.Drawing.Size(116, 13);
            this.CirclesShiftLabel.TabIndex = 12;
            this.CirclesShiftLabel.Text = "Смещение от центра:";
            // 
            // CirclesRangeLabel
            // 
            this.CirclesRangeLabel.AutoSize = true;
            this.CirclesRangeLabel.Location = new System.Drawing.Point(288, 100);
            this.CirclesRangeLabel.Name = "CirclesRangeLabel";
            this.CirclesRangeLabel.Size = new System.Drawing.Size(137, 13);
            this.CirclesRangeLabel.TabIndex = 11;
            this.CirclesRangeLabel.Text = "Ширина цветного кольца:";
            // 
            // CirclesLinesWidthTrackBar
            // 
            this.CirclesLinesWidthTrackBar.Location = new System.Drawing.Point(11, 237);
            this.CirclesLinesWidthTrackBar.Minimum = 1;
            this.CirclesLinesWidthTrackBar.Name = "CirclesLinesWidthTrackBar";
            this.CirclesLinesWidthTrackBar.Size = new System.Drawing.Size(154, 45);
            this.CirclesLinesWidthTrackBar.TabIndex = 10;
            this.CirclesLinesWidthTrackBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this.CirclesLinesWidthTrackBar.Value = 3;
            // 
            // CirclesLightIncrementTrackBar
            // 
            this.CirclesLightIncrementTrackBar.Location = new System.Drawing.Point(291, 180);
            this.CirclesLightIncrementTrackBar.Maximum = 20;
            this.CirclesLightIncrementTrackBar.Minimum = 1;
            this.CirclesLightIncrementTrackBar.Name = "CirclesLightIncrementTrackBar";
            this.CirclesLightIncrementTrackBar.Size = new System.Drawing.Size(130, 45);
            this.CirclesLightIncrementTrackBar.TabIndex = 9;
            this.CirclesLightIncrementTrackBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this.CirclesLightIncrementTrackBar.Value = 3;
            // 
            // CirclesAmountAccuracyTrackBar
            // 
            this.CirclesAmountAccuracyTrackBar.Location = new System.Drawing.Point(11, 124);
            this.CirclesAmountAccuracyTrackBar.Maximum = 1000;
            this.CirclesAmountAccuracyTrackBar.Minimum = 1;
            this.CirclesAmountAccuracyTrackBar.Name = "CirclesAmountAccuracyTrackBar";
            this.CirclesAmountAccuracyTrackBar.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.CirclesAmountAccuracyTrackBar.Size = new System.Drawing.Size(154, 45);
            this.CirclesAmountAccuracyTrackBar.TabIndex = 8;
            this.CirclesAmountAccuracyTrackBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this.CirclesAmountAccuracyTrackBar.Value = 5;
            // 
            // CirclesAccuracyTrackBar
            // 
            this.CirclesAccuracyTrackBar.Location = new System.Drawing.Point(11, 180);
            this.CirclesAccuracyTrackBar.Maximum = 100;
            this.CirclesAccuracyTrackBar.Minimum = 1;
            this.CirclesAccuracyTrackBar.Name = "CirclesAccuracyTrackBar";
            this.CirclesAccuracyTrackBar.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.CirclesAccuracyTrackBar.Size = new System.Drawing.Size(154, 45);
            this.CirclesAccuracyTrackBar.TabIndex = 6;
            this.CirclesAccuracyTrackBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this.CirclesAccuracyTrackBar.Value = 50;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.CirclesColorChangeModeRadioBtn);
            this.groupBox1.Controls.Add(this.CirclesLightChangeModeRadioBtn);
            this.groupBox1.Location = new System.Drawing.Point(182, 148);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(75, 77);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Меняем:";
            // 
            // CirclesColorChangeModeRadioBtn
            // 
            this.CirclesColorChangeModeRadioBtn.AutoSize = true;
            this.CirclesColorChangeModeRadioBtn.Location = new System.Drawing.Point(16, 24);
            this.CirclesColorChangeModeRadioBtn.Name = "CirclesColorChangeModeRadioBtn";
            this.CirclesColorChangeModeRadioBtn.Size = new System.Drawing.Size(50, 17);
            this.CirclesColorChangeModeRadioBtn.TabIndex = 5;
            this.CirclesColorChangeModeRadioBtn.Text = "Цвет";
            this.CirclesColorChangeModeRadioBtn.UseVisualStyleBackColor = true;
            this.CirclesColorChangeModeRadioBtn.CheckedChanged += new System.EventHandler(this.CirclesColorChangeModeRadioBtn_CheckedChanged);
            // 
            // CirclesLightChangeModeRadioBtn
            // 
            this.CirclesLightChangeModeRadioBtn.AutoSize = true;
            this.CirclesLightChangeModeRadioBtn.Checked = true;
            this.CirclesLightChangeModeRadioBtn.Location = new System.Drawing.Point(16, 47);
            this.CirclesLightChangeModeRadioBtn.Name = "CirclesLightChangeModeRadioBtn";
            this.CirclesLightChangeModeRadioBtn.Size = new System.Drawing.Size(49, 17);
            this.CirclesLightChangeModeRadioBtn.TabIndex = 4;
            this.CirclesLightChangeModeRadioBtn.TabStop = true;
            this.CirclesLightChangeModeRadioBtn.Text = "Свет";
            this.CirclesLightChangeModeRadioBtn.UseVisualStyleBackColor = true;
            // 
            // CirclesShiftTrackBar
            // 
            this.CirclesShiftTrackBar.Location = new System.Drawing.Point(291, 237);
            this.CirclesShiftTrackBar.Maximum = 200;
            this.CirclesShiftTrackBar.Name = "CirclesShiftTrackBar";
            this.CirclesShiftTrackBar.Size = new System.Drawing.Size(134, 45);
            this.CirclesShiftTrackBar.TabIndex = 2;
            this.CirclesShiftTrackBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this.CirclesShiftTrackBar.Value = 100;
            // 
            // CirclesRangeTrackBar
            // 
            this.CirclesRangeTrackBar.Enabled = false;
            this.CirclesRangeTrackBar.Location = new System.Drawing.Point(291, 124);
            this.CirclesRangeTrackBar.Maximum = 100;
            this.CirclesRangeTrackBar.Name = "CirclesRangeTrackBar";
            this.CirclesRangeTrackBar.Size = new System.Drawing.Size(130, 45);
            this.CirclesRangeTrackBar.TabIndex = 1;
            this.CirclesRangeTrackBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this.CirclesRangeTrackBar.Value = 10;
            // 
            // CirclesDescriptionLabel
            // 
            this.CirclesDescriptionLabel.AutoSize = true;
            this.CirclesDescriptionLabel.Location = new System.Drawing.Point(11, 9);
            this.CirclesDescriptionLabel.Name = "CirclesDescriptionLabel";
            this.CirclesDescriptionLabel.Size = new System.Drawing.Size(388, 78);
            this.CirclesDescriptionLabel.TabIndex = 0;
            this.CirclesDescriptionLabel.Text = resources.GetString("CirclesDescriptionLabel.Text");
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(468, 361);
            this.Controls.Add(this.PolygonPanel);
            this.Controls.Add(this.CirclesPanel);
            this.Controls.Add(this.GlutPanel);
            this.Controls.Add(this.ApplyButton);
            this.Controls.Add(this.RenderingMethodsList);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SettingsForm";
            this.Text = "Настройки";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SettingsForm_FormClosing);
            this.GlutPanel.ResumeLayout(false);
            this.GlutPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GlutCircleAccuracyTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GlutStacksUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GlutSlicesUpDown)).EndInit();
            this.PolygonPanel.ResumeLayout(false);
            this.PolygonPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PolygonsAccuracyTrackBar)).EndInit();
            this.CirclesPanel.ResumeLayout(false);
            this.CirclesPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CirclesLinesWidthTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CirclesLightIncrementTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CirclesAmountAccuracyTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CirclesAccuracyTrackBar)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CirclesShiftTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CirclesRangeTrackBar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox RenderingMethodsList;
        private System.Windows.Forms.Button ApplyButton;
        private System.Windows.Forms.ToolTip ToolTip;
        private System.Windows.Forms.Panel GlutPanel;
        private System.Windows.Forms.NumericUpDown GlutStacksUpDown;
        private System.Windows.Forms.NumericUpDown GlutSlicesUpDown;
        private System.Windows.Forms.CheckBox GlutFillModeCheckBox;
        private System.Windows.Forms.Label GlutDescriptionLabel;
        private System.Windows.Forms.Panel PolygonPanel;
        private System.Windows.Forms.Label PolygonsDescriptionLabel;
        private System.Windows.Forms.Panel CirclesPanel;
        private System.Windows.Forms.Label CirclesDescriptionLabel;
        private System.Windows.Forms.TrackBar PolygonsAccuracyTrackBar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton CirclesColorChangeModeRadioBtn;
        private System.Windows.Forms.RadioButton CirclesLightChangeModeRadioBtn;
        private System.Windows.Forms.TrackBar CirclesShiftTrackBar;
        private System.Windows.Forms.TrackBar CirclesRangeTrackBar;
        private System.Windows.Forms.TrackBar CirclesAmountAccuracyTrackBar;
        private System.Windows.Forms.TrackBar CirclesAccuracyTrackBar;
        private System.Windows.Forms.ColorDialog SelectColorDialog;
        private System.Windows.Forms.TrackBar CirclesLightIncrementTrackBar;
        private System.Windows.Forms.TrackBar CirclesLinesWidthTrackBar;
        private System.Windows.Forms.CheckBox GlutCircleCheckBox;
        private System.Windows.Forms.TrackBar GlutCircleAccuracyTrackBar;
        private System.Windows.Forms.Label GlutSlicesLabel;
        private System.Windows.Forms.Label GlutStacksLabel;
        private System.Windows.Forms.Label GlutAccuracyLabel;
        private System.Windows.Forms.Label PolygonsAccuracyLabel;
        private System.Windows.Forms.Label CirclesAccuracyLabel;
        private System.Windows.Forms.Label CirclesLinesWidthLabel;
        private System.Windows.Forms.Label CirclesShiftLabel;
        private System.Windows.Forms.Label CirclesRangeLabel;
        private System.Windows.Forms.Label CirclesLightIncrementLabel;
        private System.Windows.Forms.Label CirclesAmountAccuracyLabel;
    }
}