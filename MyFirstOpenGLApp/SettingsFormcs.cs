﻿using System;
using System.Windows.Forms;

namespace ru.kozalo.ismart.cone
{
    public partial class SettingsForm : Form
    {
        private readonly Lab2 _creator;

        public SettingsForm(Lab2 parent)
        {
            InitializeComponent();

            RenderingMethodsList.SelectedIndex = 1;
            _creator = parent;

            PolygonPanel.Location = GlutPanel.Location;
            CirclesPanel.Location = GlutPanel.Location;
        }

        private void RenderingMethodsList_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (RenderingMethodsList.SelectedIndex)
            {
                case 0:
                    PolygonPanel.Visible = false;
                    CirclesPanel.Visible = false;
                    GlutPanel.Visible = true;
                    break;
                case 1:
                    GlutPanel.Visible = false;
                    CirclesPanel.Visible = false;
                    PolygonPanel.Visible = true;
                    break;
                case 2:
                    GlutPanel.Visible = false;
                    PolygonPanel.Visible = false;
                    CirclesPanel.Visible = true;
                    break;
                default:
                    MessageBox.Show("Выбран неизвестный метод построения!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            RenderingParams renderingParams;

            switch (RenderingMethodsList.SelectedIndex)
            {
                case 0:
                    renderingParams = new RenderingParams();
                    renderingParams.Method = RenderingMethod.Glut;
                    renderingParams.LinesOnly = GlutFillModeCheckBox.Checked;
                    renderingParams.WithCircle = GlutCircleCheckBox.Checked;
                    renderingParams.Slices = (int) GlutSlicesUpDown.Value;
                    renderingParams.Stacks = (int) GlutStacksUpDown.Value;
                    if (GlutCircleCheckBox.Checked) renderingParams.Accuracy = (float) GlutCircleAccuracyTrackBar.Value / 100;
                    _creator.ChangeRenderingMethod(renderingParams);
                    break;
                case 1:
                    renderingParams = new RenderingParams();
                    renderingParams.Method = RenderingMethod.Polygons;
                    renderingParams.Accuracy = (float) PolygonsAccuracyTrackBar.Value / 100;
                    _creator.ChangeRenderingMethod(renderingParams);
                    break;
                case 2:
                    renderingParams = new RenderingParams();
                    renderingParams.Method = RenderingMethod.Circles;
                    renderingParams.Shift = (float) (CirclesShiftTrackBar.Value - CirclesShiftTrackBar.Maximum / 2) / 100;
                    renderingParams.LinesWidth = CirclesLinesWidthTrackBar.Value;
                    renderingParams.Accuracy = (float) CirclesAccuracyTrackBar.Value / 100;
                    renderingParams.CirclesAccuracy = (float) CirclesAmountAccuracyTrackBar.Value / 1000;
                    renderingParams.ColorInsteadLight = CirclesColorChangeModeRadioBtn.Checked;
                    if (CirclesColorChangeModeRadioBtn.Checked) {
                        renderingParams.Colors3F = new float[] { (float) SelectColorDialog.Color.R / 255, (float) SelectColorDialog.Color.G / 255, (float) SelectColorDialog.Color.B / 255 };
                        renderingParams.Range = (float) CirclesRangeTrackBar.Value / 100;
                    } else renderingParams.LightIncrement = (float) CirclesLightIncrementTrackBar.Value / 10000;
                    _creator.ChangeRenderingMethod(renderingParams);
                    break;
                default:
                    MessageBox.Show("Неизвестный метод построения!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
            }
        }

        private void SettingsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();
        }

        private void CirclesColorChangeModeRadioBtn_CheckedChanged(object sender, EventArgs e)
        {
            if (CirclesColorChangeModeRadioBtn.Checked)
            {
                if (SelectColorDialog.ShowDialog() != DialogResult.OK)
                    CirclesLightChangeModeRadioBtn.Checked = true;
                else
                {
                    CirclesLightIncrementTrackBar.Enabled = false;
                    CirclesRangeTrackBar.Enabled = true;
                }
            }
            else
            {
                CirclesRangeTrackBar.Enabled = false;
                CirclesLightIncrementTrackBar.Enabled = true;
            }
        }

        private void GlutCircleCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            GlutCircleAccuracyTrackBar.Enabled = GlutCircleCheckBox.Checked;
        }
    }
}
